<?php
$page = htmlspecialchars($_SERVER['PHP_SELF']);


class Product{
    public $naam;
    public $prijs;
}

class Lijst {
    public $lijstNaam;
    //public $pl;
    public $pl = array();
    
    public function Add($product){
       $this->pl[] = $product;
        
    }
}



$Fulllijst = new Lijst();

/*

$tmpProduct = new Product();
$tmpProduct->naam = "fiets";
$tmpProduct->prijs = 20;

$Fulllijst->Add($tmpProduct);

$tmpProduct2 = new Product();
$tmpProduct2->naam = "auto";
$tmpProduct2->prijs = 500;

$Fulllijst->Add($tmpProduct2);

*/

if (isset($_GET['clear'])){
    unset($_COOKIE["myCookie"]);
    setcookie("myCookie", null, -1);
}



if (isset($_COOKIE['myCookie'])){
    $Fulllijst = unserialize($_COOKIE['myCookie']);
}


if (isset($_POST['txtNaam']) && isset($_POST['txtPrijs'])) {
    $tmpProduct = new Product();
    $tmpProduct->naam = $_POST['txtNaam'];
    $tmpProduct->prijs = $_POST['txtPrijs'];
    $Fulllijst->Add($tmpProduct);
   
    setcookie('myCookie', serialize($Fulllijst), time()+60*60*24*15);
}


if (isset($_GET['del'])){
    unset($Fulllijst->pl[$_GET['del']]);
    setcookie('myCookie', serialize($Fulllijst), time()+60*60*24*15);
}



/*
echo "<pre>";
var_dump($Fulllijst);
echo "</pre>";
*/

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>cookies en 2 objecten</title>
</head>
<body>
    <form action="<?php echo $page;?>" method="post">
        <div>
            <label for="txtNaam">Naam:</label>
            <input type="text" name="txtNaam"/>
        </div>
        <div>
            <label for="txtPrijs">Prijs:</label>
            <input type="text" name="txtPrijs"/>
        </div>
        <div>
            <button type='submit'>Voeg Toe</button>
        </div>
    </form>
    <a href="?clear">maak leeg</a><br/>
   
        <?php
        
            if (count($Fulllijst->pl) > 0){
               foreach ($Fulllijst->pl as $key => $p) {
                   echo $p->naam . " - " . $p->prijs . "<a href='?del=$key'>delete</a><br/>";
               } 
            }
            
        ?>
   
    
</body>
</html>