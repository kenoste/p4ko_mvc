<?php




if(isset($_GET['clear'])) {
    setcookie("lijst", null, -1);
    unset($_COOKIE["lijst"]);
}


$page = htmlspecialchars($_SERVER['PHP_SELF']);

if(isset($_POST['product'])) {
    if(isset($_COOKIE['lijst'])) {
        $lijst = unserialize($_COOKIE['lijst']);
    }
    
    $lijst[] = $_POST['product'];
    
    setcookie('lijst', serialize($lijst), time()+60*60*24*15);
    
    header("Location: " . $page);
    //echo(var_dump($lijst));
} else {
    if(isset($_COOKIE['lijst'])) {
        $lijst = unserialize($_COOKIE['lijst']);
    }
}


if(isset($_GET['del'])) {
    unset($lijst[$_GET['del']]);
    setcookie('lijst', serialize($lijst), time()+60*60*24*15);
}




?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>mijn cookies oefening</title>
</head>
<body>
    <form action="cookies.php" method="post">
        <label for="product"></label>
        <input type="text" name="product" id="product"/>
        <button type="submit">Voeg Toe</button>
    </form>
    <a href='?clear=1'>leeg</a><br/><br/>
    <?php
        if (count($lijst) > 0){
            foreach($lijst as $key => $p)
            {
                echo $p . " <a href='?del=$key'>del</a><br/>";
            }
        }
    
    ?>
</body>
</html>