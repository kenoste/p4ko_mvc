<?php

session_start();
$page = htmlspecialchars($_SERVER['PHP_SELF']);

if (isset($_SESSION['lijst'])){
    $lijst = unserialize($_SESSION['lijst']);
}

if (isset($_POST['product'])) {
    $lijst[] = $_POST['product'];
}

if (isset($_GET['clear'])) {
    session_unset(); 
    session_destroy();
    header("Location: " . $page);
}

if (isset($_GET['del'])) {
    unset($lijst[$_GET['del']]);
    $_SESSION['lijst'] = serialize($lijst);
    header("Location: " . $page);
}


$_SESSION['lijst'] = serialize($lijst);



?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>mijn sessin oefening</title>
</head>
<body>
    <form action="" method="post">
        <label for="product"></label>
        <input type="text" name="product" id="product"/>
        <button type="submit">Voeg Toe</button>
    </form>
    <br/><br/>
    <?php
        if (count($lijst) > 0) {
            foreach ($lijst as $key => $p){
                echo $p . "<a href='?del=$key'>delete</a><br/>";
            }
        }
    ?>
    <br/><br/>
    <a href='?clear=1'>Leeg maken</a>
    
</body>
</html>