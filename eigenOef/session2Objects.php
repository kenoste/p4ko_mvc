<?php
session_start();
$page = htmlspecialchars($_SERVER['PHP_SELF']);

class Product {
    public $naam;
    public $prijs;
}

class Lijst extends Product {
    public $items;
    
    public function Add() {
        $newItem = new Product();
        $newItem->naam = $this->naam;
        $newItem->prijs = $this->prijs;
        $this->items[] = $newItem;
     
    }
}


$mLijst = new Lijst();


if (isset($_SESSION['lijst'])) {
    $mLijst = unserialize($_SESSION['lijst']);
}


if (isset($_POST['txtNaam']) && isset($_POST['txtPrijs'])) {
    $mLijst->naam = $_POST['txtNaam'];
    $mLijst->prijs = $_POST['txtPrijs'];
    $mLijst->Add();
    
    $_SESSION['lijst'] = serialize($mLijst);
}

if (isset($_GET['clear']))
{
    session_unset(); 
    session_destroy(); 
    header("Location: " . $page);
}

if (isset($_GET['del']))
{
    unset($mLijst->items[$_GET['del']]);
    $_SESSION['lijst'] = serialize($mLijst);
}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Session items with 2 classes</title>
</head>
<body>
    <br/>
    <form action="<?php echo $page;?>" method="post">
        <div>
            <label for="txtNaam">Naam:</label>
            <input type="text" name="txtNaam"/>
        </div>
        <div>
            <label for="txtPrijs">Prijs:</label>
            <input type="text" name="txtPrijs"/>
        </div>
        <div>
            <button type='submit'>Voeg Toe</button>
        </div>
    </form>
    <br/>
    <a href="?clear">maak leeg</a>
    <br/>
    <?php
        if (count($mLijst->items) > 0){
            foreach ($mLijst->items as $key => $tmpItem) {
                echo $tmpItem->naam . " - " . $tmpItem->prijs;
                echo "<a href='?del=$key'>delete</a><br/>";
            }
        }
        
    ?>
   
</body>
</html>