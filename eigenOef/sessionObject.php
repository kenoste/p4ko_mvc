<?php
session_start();
$page = htmlspecialchars($_SERVER['PHP_SELF']);

class Product{
    protected $naam;
    protected $prijs;
    
    public function getNaam() {
        return $this->naam;
    }
    public function setNaam($_naam) {
        if (empty($_naam)) {
            global $naamError;
            $naamError = "Naam mag niet leeg zijn";
            return false;
        }
        $naamPattern = '/^[A-Za-zéçè ]*$/';
        if (!preg_match($naamPattern, $_naam))
        {
            global $naamError;
            $naamError = "Naam moet uit letters bestaan";
            return false;
        }
        $this->naam = $_naam;    
        return true;
    }
    
    public function getPrijs() {
        return $this->prijs;
    }
    public function setPrijs($_prijs) {
        if (empty($_prijs)) {
            global $prijsError;
            $prijsError = "Prijs mag niet leeg zijn";
            return false;
        }
        if (!is_numeric($_prijs)){
            
            global $prijsError;
            $prijsError = "Prijs moet een getal zijn";
            return false;
        }
        $this->prijs = $_prijs;    
        return true;
        
    }
}

$lijst = array();

if (isset($_SESSION['lijst'])){
    $lijst = unserialize($_SESSION['lijst']);
}


if (isset($_POST['productnaam']) && isset($_POST['productprijs'])){
    $newProduct = new Product();
    
    $swOK = true;
    if (!$newProduct->setNaam($_POST['productnaam'])){
        $swOK = false;
    }
    if (!$newProduct->setPrijs($_POST['productprijs'])){
        $swOK = false;
    }
    if ($swOK == true){
        $lijst[] = $newProduct;    
    }
}

if (isset($_GET['del'])){
    unset($lijst[$_GET['del']]);
    //header("Location: " . $page);
}

$_SESSION['lijst'] = serialize($lijst);

if (isset($_GET['clear'])){
    session_unset();
    session_destroy(); 
    header("Location: " . $page);
}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>mijn session object</title>
</head>
<body>
    <form action="" method="post">
        <div>
            <label for="productnaam">naam: </label>
            <input type="text" name="productnaam"/>
            <span><?php echo $naamError;?></span>
        </div>
        <div>
            <label for="productprijs">prijs:</label>
            <input type="text" name="productprijs"/>
            <?php echo $prijsError; ?>
        </div>
            <div>
            <button type='submit'>voeg toe</button>
        </div>
    </form>
    <br/><br/>
    <a href='?clear=1'>leeg maken</a><br/>
    <?php
        
        if(count($lijst) > 0){
            foreach($lijst as $key => $p){
                echo $p->getNaam() . " - " . $p->getPrijs() . " <a href='?del=$key'>delete</a><br/>";
            }   
        }
    ?>
        
</body>
</html>