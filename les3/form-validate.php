<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <?php
    
    $productErr = $prijsErr = $prijs = $product = "";
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
        if (empty($_POST["product"])) {
            $productErr = "Product is required dude";
        } else {
             $product = test_input($_POST["name"]);
        }
        
        if (empty($_POST["prijs"])) {
            $prijsErr = "Prijs is required dude";
        } else {
             
            $prijs = test_input($_POST["prijs"]);
            $pricePattern = '/^[0-9]+(\.[0-9]{1,2})?$/';
        
            if (!preg_match($pricePattern, $price)) {
                $prijsErr = 'Typ een geldig eurogetal in! Bv. 99.00';
            }
        }

    }
    
    function is_decimal( $val ){
        return is_numeric( $val ) && floor( $val ) != $val;
    }
    
    function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
    }
    
    ?>
    
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <input type="text" name="product" value=<?php echo $product ?>><br/>
        <span class="error">* <?php echo $productErr;?></span><br/>
        <input type="text" name="prijs" value=<?php echo $prijs ?>><br/>
        <span class="error">* <?php echo $prijsErr;?></span><br/>
        <input type="submit" value="Submit"/>
    </form>
    
</body>
</html>