<?php

if (isset($_POST['voornaam'])) {
        $voornaam = $_POST['voornaam'];
        
        if (empty($voornaam)) {
            $voornaamErr = 'Voornaam moet ingevuld zijn!';
        } else {
            $naamPattern = '/^[a-zA-Zéç ]*$/';
            if (!preg_match($naamPattern, $voornaam)) {
                $voornaamErr = 'Voornaam bestaat enkel uit karakters alleen!';
            } 
        }
}

if (isset($_POST['naam'])) {
        $naam = $_POST['naam'];
        
        if (empty($naam)) {
            $naamErr = 'Naam moet ingevuld zijn!';
        } else {
            $naamPattern = '/^[a-zA-Zéç ]*$/';
            if (!preg_match($naamPattern, $naam)) {
                $naamErr = 'Naam bestaat enkel uit karakters alleen!';
            } 
        }
}




if (isset($_POST['plaats'])) {
        $plaats = $_POST['plaats'];
        switch ($plaats) {
            case 0 :
                 $plaatsText = 'Stage';
                break;
            case 1 :
                $plaatsText = 'Tribune';
                break;        
            case 2 :
                $plaatsText = 'Balkon';
                break;
            default :
                $plaatsText = 'onbekend';
        }
    } 
 
 
 
if (isset($_POST['aantal'])) {
        $aantal = $_POST['aantal'];
        
        if (empty($aantal)) {
            $aantalErr = 'Aantal moet ingevuld zijn!';
        } else {
            /*
            if (!is_numeric($aantal)) {
                $aantalErr = 'Aantal moet een getal zijn!';
            }
            */
            
            $aantalPattern = '/^[0-9]*$/';
            if (!preg_match($aantalPattern, $aantal)) {
                $aantalErr = 'Aantal moet een geldig nr zijn!';
            } 
            
        }
        
}

if (isset($_POST['gebDatum'])) {
        $gebDatum = $_POST['gebDatum'];
        
        if (empty($gebDatum)) {
            $gebDatumErr = 'Datum moet ingevuld zijn!';
        }
}

if (isset($_POST['rekNr'])) {
        $rekNr = $_POST['rekNr'];
        
        if (empty($rekNr)) {
            $rekNrErr = 'Rekening moet ingevuld zijn!';
        } else {
             if (!checkIBAN($rekNr)){
                 $rekNrErr = "Rekeningnummer moet geldig zijn!";
             }
        }
}

if (isset($_POST['email'])) {
        $email = $_POST['email'];
        
         if (empty($email)) {
            $emailErr = 'Email moet ingevuld zijn!';
        } else {
             $emailPattern = '/^[^@]+@[^@]+\.[^@]+$/';
            if (!preg_match($emailPattern, $email)) {
                $emailErr = 'Moet een geldig Email adres zijn!';
            } 
        }
}
 

function checkIBAN($iban)
{
    $iban = strtolower(str_replace(' ','',$iban));
    $Countries = array('al'=>28,'ad'=>24,'at'=>20,'az'=>28,'bh'=>22,'be'=>16,'ba'=>20,'br'=>29,'bg'=>22,'cr'=>21,'hr'=>21,'cy'=>28,'cz'=>24,'dk'=>18,'do'=>28,'ee'=>20,'fo'=>18,'fi'=>18,'fr'=>27,'ge'=>22,'de'=>22,'gi'=>23,'gr'=>27,'gl'=>18,'gt'=>28,'hu'=>28,'is'=>26,'ie'=>22,'il'=>23,'it'=>27,'jo'=>30,'kz'=>20,'kw'=>30,'lv'=>21,'lb'=>28,'li'=>21,'lt'=>20,'lu'=>20,'mk'=>19,'mt'=>31,'mr'=>27,'mu'=>30,'mc'=>27,'md'=>24,'me'=>22,'nl'=>18,'no'=>15,'pk'=>24,'ps'=>29,'pl'=>28,'pt'=>25,'qa'=>29,'ro'=>24,'sm'=>27,'sa'=>24,'rs'=>22,'sk'=>24,'si'=>19,'es'=>24,'se'=>24,'ch'=>21,'tn'=>24,'tr'=>26,'ae'=>23,'gb'=>22,'vg'=>24);
    $Chars = array('a'=>10,'b'=>11,'c'=>12,'d'=>13,'e'=>14,'f'=>15,'g'=>16,'h'=>17,'i'=>18,'j'=>19,'k'=>20,'l'=>21,'m'=>22,'n'=>23,'o'=>24,'p'=>25,'q'=>26,'r'=>27,'s'=>28,'t'=>29,'u'=>30,'v'=>31,'w'=>32,'x'=>33,'y'=>34,'z'=>35);

    if(strlen($iban) == $Countries[substr($iban,0,2)]){

        $MovedChar = substr($iban, 4).substr($iban,0,4);
        $MovedCharArray = str_split($MovedChar);
        $NewString = "";

        foreach($MovedCharArray AS $key => $value){
            if(!is_numeric($MovedCharArray[$key])){
                $MovedCharArray[$key] = $Chars[$MovedCharArray[$key]];
            }
            $NewString .= $MovedCharArray[$key];
        }

        if(bcmod($NewString, '97') == 1)
        {
            return TRUE;
        }
        else{
            return FALSE;
        }
    }
    else{
        return FALSE;
    }   
}


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        label {
            display: inline-block;
            width: 120px;
        }
        input, select {
            width: 200px;
        }
    </style>
</head>
<body>
    
    <h1>Bob Dylan</h1>
    
   
    
     <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
        <div>
            <label for="voornaam">voornaam</label>
            <input id="voornaam" name="voornaam" type="text">
            <span><?php echo $voornaamErr;?></span>
       </div>
       <div>
            <label for="naam">naam</label>
            <input id="naam" name="naam" type="text">
            <span><?php echo $naamErr;?></span>
        </div>
        <div>
            <label for="plaats">Plaats</label>
            <select name="plaats" id="plaats">
                <option value="0">Stage</option>
                <option value="1">Tribune</option>
                <option value="2">Balkon</option>
            </select>
         </div>
         <div>
            <label for="aantal">Aantal</label>
            <input id="aantal" name="aantal" type="text">
            <span><?php echo $aantalErr;?></span>
         </div>
         <div>
            <label for="gebDatum">Geboorte datum</label>
            <input id="gebDatum" name="gebDatum" type="date">
            <span><?php echo $gebDatumErr;?></span>
         </div>  
         <div>
            <label for="rekNr">Rekeningnummer</label>
            <input id="rekNr" name="rekNr" type="text" placeholder="GB82 WEST 1234 5698 7654 32">
            <span><?php echo $rekNrErr;?></span>
         </div>
         <div>
            <label for="email">Email</label>
            <input id="email" name="email" type="text" placeholder="ik@thuis.be">
            <span><?php echo $emailErr;?></span>
         </div> 
         <button type=submit>Verzenden</button>
    </form>
    <br/>
    <br/>
    

<?php

echo "naam: " . $naam . "<br/>";
echo "voornaam: " . $voornaam . "<br/>";
echo "plaats: " . $plaats . "<br/>";
echo "plaatsText: " . $plaatsText . "<br/>";
echo "Aantal: " . $aantal . "<br/>";
echo "Geboorte datum: " . $gebDatum . "<br/>";
echo "Rekeningnummer: " . $rekNr . "<br/>";
echo "Email: " . $email . "<br/>";

?>

</body>
</html>