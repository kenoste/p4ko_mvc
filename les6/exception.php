<?php
function checkNum($number) {
    if (!filter_var($number, FILTER_VALIDATE_INT)) {
        throw new \Exception('De waarde is geen geheel getal'); 
    }
}


try {
    checkNum("1e");    
} catch (\Exception $exception) {
    echo $exception->getMessage();
}
