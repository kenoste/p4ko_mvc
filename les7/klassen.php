<?php
namespace Kenoste;

$cookieName = "noticeBoard";
$noticeboard;
$foundNotices;

$page = htmlspecialchars($_SERVER['PHP_SELF']);

class Notice {
    public $id;
    public $text;
    public $owner;
    public $title;
    public $publishedOn;
    public $modifiedOn;
}

class NoticeBoard extends Notice {
    public $notices;

    private $currentNotice;


    public function paste() {
        $newNotice = new Notice();
        $newNotice->text = $this->text;
        $newNotice->owner = $this->owner;
        $newNotice->title = $this->title;

        $this->notices[] = $newNotice;
    }

    public function searchNotice ($searchString) {
        $foundNotices = null;
        foreach ($this->notices as $tmpNotice) {
            if (
                (strpos($tmpNotice->title, $searchString) !== false) ||
                (strpos($tmpNotice->text, $searchString) !== false) ||
                (strpos($tmpNotice->owner, $searchString) !== false)
            ) {
                $foundNotices[] = $tmpNotice;
            }
        }
        return $foundNotices;
    }


}

if (isset($_COOKIE[$cookieName])){


    $noticeboard = unserialize($_COOKIE[$cookieName]);
}
else
{
    $noticeboard = new \kenoste\NoticeBoard();

    $noticeboard->text = "mijn notitie";
    $noticeboard->owner = "Ken Oste";
    $noticeboard->title = "post1";
    $noticeboard->paste();

    $noticeboard->text = "mijn tweede notitie";
    $noticeboard->owner = "Ken Oste";
    $noticeboard->title = "post2";
    $noticeboard->paste();

}


if (isset($_GET['rmAll'])) {
    //setcookie($cookieName, null, -1);
    setcookie($cookieName, null, -1, "/");
    header("Location: " . $page);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['zoek']) && $_POST['zoek'] != "" ) {

        //print_r($ret);
        $ret = $noticeboard->searchNotice($_POST['zoek']);
        //print_r($ret);
    } else {
        $ret = "";
    }

    if ((isset($_POST['naam'])) && (isset($_POST['titel'])) && (isset($_POST['text'])) &&
        ($_POST['naam'] != "") && ($_POST['titel'] != "") && ($_POST['text'] != "")) {

        //echo "OKOKOKOKOKOKOKOK";

        $noticeboard->owner = $_POST['naam'];

        $noticeboard->title = $_POST['titel'];

        $noticeboard->text = $_POST['text'];

        $noticeboard->paste();

        setcookie($cookieName, serialize($noticeboard), time() + (86400 * 7), "/");

    }

}



?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Werken met klassen in PHP</title>

    <style>

        form textarea + label {
            float: left;
        }

        label {
            display: inline-block;
            width: 80px;
        }

        input, textarea {
            width: 300px;
        }

    </style>

</head>
<body>
<?php
echo "<table border='1'>";
echo "<tr><th>Title</th><th>Owner</th><th>text</th></tr>";
foreach ($noticeboard->notices as $notice) {
    echo "<tr>";
    echo "<td>" . $notice->title . "</td>";
    echo "<td>" . $notice->owner . "</td>";
    echo "<td>" . $notice->text . "</td>";
    echo "</tr>";
}
echo "</table>";

echo "<br/><br/><br/>";

if ($ret != "") {
    echo "Gevonden:<br/>";
    echo "<table border='1'>";
    foreach ($ret as $notice) {
        echo "<tr>";
        echo "<td>" . $notice->title . "</td>";
        echo "<td>" . $notice->owner . "</td>";
        echo "<td>" . $notice->text . "</td>";
        echo "</tr>";
    }
    echo "</table>";
}



?>
<br/><br/>
<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
    <div>
        <label for="naam">Naam:</label>
        <input id="naam" name="naam" type="text" >
    </div>
    <div>
        <label for="titel">Titel:</label>
        <input id="titel" name="titel" type="text" >
    </div>
    <div>
        <textarea name="text" rows="5" cols="40"></textarea>
        <label for="text">Text:</label>
    </div>
    <div>
        <label for="zoek">Zoek:</label>
        <input id="zoek" name="zoek" type="text" >
    </div>
    <div>
        <button type="submit">Plak</button>
    </div>
</form>

<a href="klassen.php?rmAll='true'">empty</a>

</body>
</html>