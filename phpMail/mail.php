<?php

require_once "../vendor/autoload.php";

$mail = new PHPMailer;
//Set PHPMailer to use SMTP.
$mail->isSMTP();

//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;

//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';

//Set SMTP host name
$mail->Host = "smtp.gmail.com";
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6

//Set this to true if SMTP host requires authentication to send email
$mail->SMTPAuth = true;
//Provide username and password
$mail->Username = "developerjef@gmail.com";
$mail->Password = "petillant20";
//If SMTP requires TLS encryption then set it
$mail->SMTPSecure = "ssl";
//Set TCP port to connect to
$mail->Port = 465;

$mail->From = "kenoste@gmail.com";
$mail->FromName = "Ken Oste";

$mail->addAddress("kenoste@gmail.com", "Ken Oste");

$mail->isHTML(true);

$mail->Subject = "Hallo vanuit php";
$mail->Body = "<i>Dit is mijn body, ook uit php</i>";
$mail->AltBody = "This is the plain text version of the email content";

if(!$mail->send())
{
    echo "Mailer Error: " . $mail->ErrorInfo;
}
else
{
    echo "Message has been sent successfully";
}