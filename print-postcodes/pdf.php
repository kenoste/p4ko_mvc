<?php
require('../vendor/setasign/fpdf/fpdf.php');

class PDF extends FPDF
{
// Page header
function Header()
{
    // Logo
    //$this->Image('logo.png',10,6,30);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30,10,'PostCodes',1,0,'C');
    // Line break
    $this->Ln(20);
}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}
}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);

// for($i=1;$i<=40;$i++)
//     $pdf->Cell(0,10,'Printing line number '.$i,0,1);
  
$fileContent = file_get_contents("data/Postcodes.csv");
$fileContent= utf8_encode($fileContent);
$fileLines= explode("\n", $fileContent);

// foreach ($fileLines as $line) {
//     $pdf->Cell(0,10,$line,1);
//     $pdf->Ln();
// }

// $fileContent="1000|BRUSSEL|Brussel (19 gemeenten)|BRUXELLES|Bruxelles (19 communes)\n
// 1005|Verenigde Vergadering van de Gemeenschappelijke ||Assemblée Réunie de la Commission Communautaire|\n
// 1006|Raad van de Vlaamse Gemeenschapscommissie||Raad van de Vlaamse Gemeenschapscommissie |\n
// 1007|Assemblée de la Commission Communautaire Française||Assemblée de la Commission Communautaire Française|\n
// 1008|Kamer van Volksvertegenwoordigers||Chambre des Représentants|\n
// 1009|Belgische Senaat||Senat de Belgique|\n";
// $fileLines= explode("\n", $fileContent);

// Column widths
$w = array(40, 90, 40, 45);

foreach ($fileLines as $line) {
        
        $colls = explode("|", $line);
       // $pdf->Cell($w[$i],7,$header[$i],1,0,'C');
       //$pdf->Cell(40,7,$col[0],0,1);
       // foreach ($colls as $coll) {
            $pdf->Cell($w[0],6,$colls[0],'LR');
            $pdf->Cell($w[1],6,$colls[2],'LR');
            //$pdf->Cell($w[2],6,$colls[2],'LR',0,'R');
            //$pdf->Cell($w[3],6,$colls[3],'LR',0,'R');
            $pdf->Ln();
            
       // }
        
    }
    
        
    

// // Header
//     foreach($header as $col)
//         $this->Cell(40,7,$col,1);
//     $this->Ln();
//     // Data
//     foreach($data as $row)
//     {
//         foreach($row as $col)
//             $this->Cell(40,6,$col,1);
//         $this->Ln();
//     }
    



    
$pdf->Output();
?>